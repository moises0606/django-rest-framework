Kalenders ,
This project has been made using React-Redux & Django-rest-framework
INTERFAZ--------------------------
Cuando accedemos a la Aplicacion Web, podremos observar que en la parte de arriba
esta el menu, desde aquí podremos acceder a los diferentes modulos.
En este caso los modulos que estan editados son contact, calendars, profile y auth.

----------------------------Calendars
    Cuando entramos aqui nos apareceran todos los calendarios, si hay muchos apareceran paginados,
    en caso de que estemos logueados, podremos cambiar, entre todos los calendarios o los calendarios
    creados por nosotros
        -------------------Calendario
        Cuando entramos al calendario, nos aparecerá un meta en el cual podremos ver el nombre del
        calendario, el creador y la fecha de creación, en caso de que estemos loggeados y sea un calendario
        creado por nosotros, podremos eliminar el calendario.
        Mas abajo podremos ver la informacion del calendario, es decir el horario en el que estará disponible
        el creador del calendario para recibir citas (posible mejora en caso de haber tenido mas tiempo, sería 
        poder suscribirse al calendario, el dia y hora seleccionados, en caso de estar logeeado y no ser el creador)

----------------------------Profile
    En caso de estar loggeado y acceder al profile, podremos ver por defecto los articulos creados, pero lo importante
    viene en que podemos reutilizar el componente de calendars, a este le enviamos un "type" y este no nos mostrará la 
    navegación y ademas nos mostrará los calendarios a los que estamos suscritos o nuestros calendarios, los mostrará
    paginados, tambien podemos entrar al calendario en caso de querer acceder a el

----------------------------LOGIN
    SIGN IN
    Desde el sign in podemos loggearnos, introducimos el email, y contraseña
    Si se ha realizado con exito, mostrará un toastr y redireccionara al Home
    SIGN IN SOCIAL
    Podremos loggearnos clicando en la red social de google. Podremos loggearnos desde el propio
    sign in o incluso desde el sign up, sin condición de que ya hayamos accedido previamente a traves de
    google.
    SIGN UP
    Desde el sign up nos podremos registrar, siempre se realizará como usuario normal (user)
    Si se ha realizado con exito
    LOGOUT
    Cuando pulsamos logout, desde el settings, se destruirá la sesion del usuario.

----------------------------Contact
    En el contact podemos escribir nuestro email, el asunto y el mensaje, este lo validará tanto en cliente,
    como en servidor, mostrará los errores y si está todo bien, enviará el email