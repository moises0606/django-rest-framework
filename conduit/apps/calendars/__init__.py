from django.apps import AppConfig


class CalendarsAppConfig(AppConfig):
    name = 'conduit.apps.calendars'
    label = 'calendars'
    verbose_name = 'Calendars'

    def ready(self):
        import conduit.apps.calendars.signals

default_app_config = 'conduit.apps.calendars.CalendarsAppConfig'
