from django.contrib import admin
from .models import Calendar, CalendarSuscribe

admin.site.register(Calendar)
admin.site.register(CalendarSuscribe)
