from django.db import models
from conduit.apps.core.models import TimestampedModel

class Calendar(TimestampedModel):
    slug = models.SlugField(db_index=True, max_length=255, unique=True)
    name = models.CharField(db_index=True, max_length=255)
    normalTime = models.CharField(db_index=True, max_length=255) #avaliablity range of hours

    # Every calendar must have an author. This will answer questions like "Who
    # gets credit for writing this calendar?" and "Who can edit this calendar?".
    # Unlike the `User` <-> `Profile` relationship, this is a simple foreign
    # key (or one-to-many) relationship. In this case, one `Profile` can have
    # many `Calendar`s.
    author = models.ForeignKey('profiles.Profile', db_index=True, on_delete=models.CASCADE, related_name='calendars')

    def __str__(self):
        return self.name

class CalendarSuscribe(TimestampedModel):
    calendar = models.ForeignKey('calendars.Calendar', db_index=True, related_name='calendarSuscribed')
    user = models.ForeignKey('profiles.Profile', db_index=True, related_name='calendarSuscribed')
    validated = models.BooleanField(default=False)
    time = models.CharField(db_index=True, max_length=255)