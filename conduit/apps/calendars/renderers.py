from conduit.apps.core.renderers import ConduitJSONRenderer


class CalendarJSONRenderer(ConduitJSONRenderer):
    object_label = 'calendar'
    pagination_object_label = 'calendars'
    pagination_count_label = 'calendarsCount'

