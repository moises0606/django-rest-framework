from rest_framework import serializers
from conduit.apps.profiles.serializers import ProfileSerializer
from .models import Calendar, CalendarSuscribe

class CalendarSerializer(serializers.ModelSerializer):
    author = ProfileSerializer(read_only=True)
    slug = serializers.SlugField(required=False)
    name = serializers.CharField(required=False)
    normalTime = serializers.CharField(required=False)

    # Django REST Framework makes it possible to create a read-only field that
    # gets it's value by calling a function. In this case, the client expects
    # `created_at` to be called `createdAt` and `updated_at` to be `updatedAt`.
    # `serializers.SerializerMethodField` is a good way to avoid having the
    # requirements of the client leak into our API.
    createdAt = serializers.SerializerMethodField(method_name='get_created_at')
    updatedAt = serializers.SerializerMethodField(method_name='get_updated_at')

    class Meta:
        model = Calendar
        fields = (
            'author',
            'createdAt',
            'slug',
            'name',
            'updatedAt',
            'normalTime'
        )

    def create(self, validated_data):
        author = self.context.get('author', None)
        tags = validated_data.pop('tags', [])
        calendar = Calendar.objects.create(author=author, **validated_data)
        return calendar

    def get_created_at(self, instance):
        return instance.created_at.isoformat()

    def get_suscribed(self, instance):
        request = self.context.get('request', None)
        if request is None:
            return False

        if not request.user.is_authenticated():
            return False

        return request.user.profile.has_favorited(instance)

    def get_updated_at(self, instance):
        return instance.updated_at.isoformat()

class CalendarSuscribeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CalendarSuscribe
        fields = ('calendar', 'user', 'validated', 'time' )

    def get_created_at(self, instance):
        return instance.created_at.isoformat()

    def get_updated_at(self, instance):
        return instance.updated_at.isoformat()
