from django.conf.urls import include, url

from rest_framework.routers import DefaultRouter

from .views import (
CalendarViewSetAdmin, CalendarViewSet, CalendarsFeedAPIView, CalendarsSuscribedAPIView
)

router = DefaultRouter(trailing_slash=False)
router.register(r'calendars', CalendarViewSet)

#Admin
router.register(r'calendars_Admin', CalendarViewSetAdmin)  

urlpatterns = [
    url(r'^', include(router.urls)),
  
    url(r'^calendars/cal/feed/?$', CalendarsFeedAPIView.as_view()),

    # url(r'^calendars/cal/(?P<calendar_slug>[-\w]+)/ss/?$', CalendarViewSet.as_view()),
    url(r'^calendars/cal/suscribed/?$', CalendarsSuscribedAPIView.as_view()),

    url(r'^calendars/(?P<calendar_slug>[-\w]+)/suscribed/?$', CalendarsSuscribedAPIView.as_view()),
]