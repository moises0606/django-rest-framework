from rest_framework import generics, mixins, status, viewsets
from rest_framework.exceptions import NotFound
from rest_framework.permissions import (
    AllowAny, IsAuthenticated, IsAuthenticatedOrReadOnly, IsAdminUser
)
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Calendar, CalendarSuscribe
from .renderers import CalendarJSONRenderer
from .serializers import CalendarSerializer

#Admin
class CalendarViewSetAdmin(viewsets.ModelViewSet):
    queryset = Calendar.objects.all()
    serializer_class = CalendarSerializer
    lookup_field = 'slug'
    permission_classes = (IsAuthenticated,)
    permission_classes = (IsAdminUser,)
    
class CalendarViewSet(mixins.CreateModelMixin, 
                     mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):
    lookup_field = 'slug'
    queryset = Calendar.objects.select_related('author', 'author__user')
    permission_classes = (IsAuthenticatedOrReadOnly,)
    renderer_classes = (CalendarJSONRenderer,)
    serializer_class = CalendarSerializer

    def get_queryset(self):
        queryset = self.queryset

        author = self.request.query_params.get('author', None)
        if author is not None:
            queryset = queryset.filter(author__user__username=author)

        return queryset

    def create(self, request):
        serializer_context = {
            'author': request.user.profile,
            'request': request
        }
        serializer_data = request.data.get('calendar', {})
        serializer = self.serializer_class(data=serializer_data, context=serializer_context)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def list(self, request):
        serializer_context = {'request': request}
        page = self.paginate_queryset(self.get_queryset())
        serializer = self.serializer_class(
            page,
            context=serializer_context,
            many=True
        )
        return self.get_paginated_response(serializer.data)

    def retrieve(self, request, slug):
        serializer_context = {'request': request}

        try:
            serializer_instance = self.queryset.get(slug=slug)
        except Calendar.DoesNotExist:
            raise NotFound('The calendar with this slug does not exist.')

        serializer = self.serializer_class(
            serializer_instance,
            context=serializer_context
        )
        return Response(serializer.data, status=status.HTTP_200_OK)


    def update(self, request, slug):
        serializer_context = {'request': request}

        try:
            serializer_instance = self.queryset.get(slug=slug)
        except Calendar.DoesNotExist:
            raise NotFound('The calendar with this slug does not exist.')
            
        serializer_data = request.data.get('article', {})
        serializer = self.serializer_class(
            serializer_instance, 
            context=serializer_context,
            data=serializer_data, 
            partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)
        
    def destroy(self, request, slug):
        try:
            calendar = Calendar.objects.get(slug=slug)
        except Calendar.DoesNotExist:
            raise NotFound('The calendar with this slug does not exist.')

        calendar.delete()
        return Response(None, status=status.HTTP_204_NO_CONTENT)


class CalendarsFeedAPIView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Calendar.objects.all()
    renderer_classes = (CalendarJSONRenderer,)
    serializer_class = CalendarSerializer

    def get_queryset(self):
        return Calendar.objects.filter(
            author__in=[self.request.user.profile]
        )
        
    def list(self, request):
        serializer_context = {'request': request}
        page = self.paginate_queryset(self.get_queryset())

        serializer = self.serializer_class(
            page,
            context=serializer_context,
            many=True
        )
        return self.get_paginated_response(serializer.data)

class CalendarsSuscribedAPIView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    renderer_classes = (CalendarJSONRenderer,)
    serializer_class = CalendarSerializer
    queryset = Calendar.objects.all()

    def get_queryset(self):
        mySuscriptions = CalendarSuscribe.objects.filter(
                user=self.request.user.profile
            ).values('calendar')
        
        calendars_id = self.paginate_queryset(mySuscriptions)
        keys = []
        for calendario in calendars_id:
            keys.append(calendario.values()[0])

        return Calendar.objects.filter(
            pk__in=keys
        )

    def list(self, request):
        serializer_context = {'request': request}
        page = self.paginate_queryset(self.get_queryset())

        serializer = self.serializer_class(
            page,
            context=serializer_context,
            many=True
        )
        return self.get_paginated_response(serializer.data)
