from rest_framework import serializers

class ContactSerializer(serializers.Serializer):
    email = serializers.CharField(max_length=255)
    subject = serializers.CharField(max_length=255)
    comment = serializers.CharField(max_length=2555)

    def validate(self, data):
        # The `validate` method is where we make sure that the current
        # instance of `LoginSerializer` has "valid". In the case of logging a
        # user in, this means validating that they've provided an email
        # and password and that this combination matches one of the users in
        # our database.

        email = data.get('email', None)
        subject = data.get('subject', None)
        comment = data.get('comment', None)

        # print "***************************************"
        # print email

        # As mentioned above, an email is required. Raise an exception if an
        # email is not provided.
        if email is None:
            raise serializers.ValidationError(
                'An email address is required to send email..'
            )

        # As mentioned above, a subject is required. Raise an exception if a
        # subject is not provided.
        if subject is None:
            raise serializers.ValidationError(
                'A subject is required to send email.'
            )

        # As mentioned above, a comment is required. Raise an exception if a
        # comment is not provided.
        if comment is None:
            raise serializers.ValidationError(
                'A comment is required to send email.'
            )

        # The `validate` method should return a dictionary of validated data.
        # This is the data that is passed to the `create` and `update` methods
        # that we will see later on.
        return {
            'email': email,
            'subject': subject,
            'comment': comment
        }