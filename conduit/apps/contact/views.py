from django.core.mail import send_mail, BadHeaderError
from rest_framework import permissions, status, views, viewsets
from django.http import HttpResponse, HttpResponseRedirect
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from .serializers import ContactSerializer

class ContactView(views.APIView):
    permission_classes = (AllowAny,)
    serializer_class = ContactSerializer

    def post(self, request, format=None):
        """
        return Response({
                    'status': 'true',
                    'message': 'Success! Thank you for your message'
                }, status=status.HTTP_200_OK)
        """

        # print '---------------------------------------'
        # print ''
        # print request.data.get('contact').get('comment')
        # print ''
        # print '---------------------------------------'
        contact = request.data.get('contact', {})

        serializer = self.serializer_class(data=contact)
        serializer.is_valid(raise_exception=True)

        email = contact.get('email', None)
        subject = contact.get('subject', None)
        message = contact.get('comment', None)


        try:
            send_mail(subject, message, 'costumer@kalender.com', [email])
        except BadHeaderError:
            return Response({
                    'status': 'false',
                    'message': 'BadHeaderError for your message'
                }, status=status.HTTP_503_SERVICE_UNAVAILABLE)

        return Response({
                    'status': 'true',
                    'message': 'Success! Thank you for your message'
                }, status=status.HTTP_200_OK)
