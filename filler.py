import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE','conduit.settings')

import django
django.setup()

from conduit.apps.articles.models import Article, Comment, Tag
from conduit.apps.profiles.models import Profile
from conduit.apps.calendars.models import Calendar, CalendarSuscribe
from conduit.apps.authentication.models import User
from faker import Faker

fake = Faker()

def call(N):
    for i in range(N):
        #ARTICLES
        fake_tag = fake.word()
        fake_slug = fake.slug()
        tag = Tag.objects.get_or_create(tag=fake_tag, slug=fake_slug)[0]
        
        fake_slug = fake.slug()
        fake_title = fake.word()
        fake_description = fake.text()
        fake_body = fake.text()
        author_yomogan = Profile.objects.get_or_create(user__username='owner')[0]
    
        a = Article.objects.get_or_create(slug=fake_slug, title=fake_title, description=fake_description, body=fake_body, author=author_yomogan)[0]
        a.tags.add(tag)
        
        fake_body = fake.text()
        c = Comment.objects.get_or_create(body=fake_body, article=a, author=author_yomogan)[0]

        # CALENDARS
        fake_slug = fake.slug()
        fake_name = fake.name()
        fake_normalTime = {"evening": {"end": "22:00", "start": "15:00"}, "morning": {"end": "13:00", "start": "20:00"}}
        fake_author = Profile.objects.get_or_create(user__username='owner')[0]
        cal = Calendar.objects.get_or_create(slug=fake_slug, name=fake_name, normalTime=fake_normalTime, author=fake_author)[0]

        fake_follower = Profile.objects.get_or_create(user__username='follower')[0]
        fake_validated = fake.boolean(chance_of_getting_true=50)
        fake_time = {"year":"2019", "month":fake.month(), "day":fake.day_of_month(), "start_hour": "12:00"}
        CalendarSuscribe.objects.get_or_create(calendar=cal, user=fake_follower, validated=fake_validated, time=fake_time)

if __name__ == '__main__':
    print("Filling random data")
    call(4)
    print("Filling done ")
    